import Vue from 'vue';
import Vuex from 'vuex';

import map from './modules/map';
import view from './modules/view';
import auth from './modules/auth';
import create from './modules/create';
import meetings from './modules/meetings';

Vue.use(Vuex);

const store = () => new Vuex.Store({

    modules: {
        view,
        map,
        auth,
        create,
        meetings
    }
})
  
export default store