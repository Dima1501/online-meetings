const togglePopup = (state, popup) => {
    state.popups.activePopup = popup
}

const toggleSidebar = (state, type) => {
    state.sidebar.type = type
}

const toggleCreate = (state, value) => {
    state.map.create = value
}

const toggleNotify = (state, message) => {
    state.notify.visible = !state.notify.visible
    state.notify.message = message
}

const togglePreloader = (state, type) => {
    state.preloader[type] = !state.preloader[type]
}

export default {
    togglePopup,
    toggleSidebar,
    toggleCreate,
    toggleNotify,
    togglePreloader
}