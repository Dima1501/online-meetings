import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  popups: {
    activePopup: ''
  },
  sidebar: {
    type: ''
  },
  map: {
    create: false,
  },
  notify: {
    visible: false,
    message: ''
  },
  preloader: {
    myMeetings: false
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};