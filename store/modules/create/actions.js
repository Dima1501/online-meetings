import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const editMeeting = ({commit, dispatch, rootState} , data) => {
  firebase.firestore().collection('moderate').doc(data.id).update({
    subtitle: data.subtitle,
    reqs: data.reqs,
    reqsLen: data.reqs.length,
    status: data.status
  }).then(() => console.log('changes sent!'))
  .catch(err => console.log(err))
}

const createMeeting = ({commit, dispatch, rootState} , data) => {
  
    firebase.firestore().collection('moderate').add({
      coords: data.coords,
      title: data.title,
      subtitle: data.subtitle,
      reqs: data.reqs,
      reqsLen: data.reqs.length,
      author: data.author,
      status: 'pending',
      id: data.id,
      peoples: 1,
      participants: [rootState.auth.user.uid]
    }).then((e) => {
      firebase.firestore().collection('moderate').doc(e.id).update({
        id: e.id
      })
  
      firebase.firestore()
      .collection('meetings')
      .doc(e.id)
      .set(
        { id: e.id, coords: data.coords, status: 'pending' }
      ).then(() => console.log('small meetings loaded'))
  
      commit('view/togglePopup', null, {root:true})
      dispatch('view/showNotify', 'Your meeting has been sent on moderation. You can check status in "My Meetings" field', {root:true})
      commit('view/toggleCreate', false, {root:true})
      commit('map/toggleMarker', false, {root:true})
  
      firebase.firestore().collection('chat').add({
        id: e.id,
        messages: []
      }).then((chat) => {
        firebase.firestore().collection('moderate').doc(e.id).update({
          chatId: chat.id
        }).then(() => console.log('fin'))
      })
    }).catch((err) => {
      console.log(err)
    })
}

export default {
  createMeeting,
  editMeeting
}