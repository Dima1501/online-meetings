import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  coords: '',
  title: '',
  subtitle: '',
  author: '',
  reqs: [],
  reqsLen: 1
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};