const toggleMarker = (state, type) => {
    state.newMarker.visible = !state.newMarker.visible
}

export default {
    toggleMarker
}