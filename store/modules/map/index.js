import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  center: [30.318519, 59.937336],
  newMarker: {
    coords: null,
    title: null,
    subtitle: null,
    reqs: [],
    visible: false
  },
  bounds: {}
};

export default {
  namespaced:   true,
  state,
  actions,
  mutations,
  getters
};