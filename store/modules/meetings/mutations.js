const writeMyMeeting = (state, data) => {
    state.myMeetings.push(data.fields)
}

const clearMyMeeting = (state, meeting) => {
    state.myMeetings = []
}

const clearModerateMeeting = (state, meeting) => {
    state.moderateMeetings = []
}

const clearMapMeeting = (state, meeting) => {
    state.mapMeetings = []
}

const setModerateMeetings = (state, data) => {
    state.moderateMeetings.push(data.fields)
}

const setMapMeetings = (state, data) => {
    state.mapMeetings.push(data.fields)
}

const togglePreviewMeeting = (state, meeting) => {
    state.previewMeeting = meeting
}

const writeChat = (state, chat) => {
    state.chat = chat
}

const clearChat = (state, data) => {
    state.chat = []
}

const addMessage = (state, data) => {
    state.chat.messages.push({
        author: data.author,
        message: data.message
    })
}

const updatePeoples = (state, data) => {
    console.log('peoples' + data.peoples)
    console.log('id' + data.id)
    console.log(state.mapMeetings)
}

const setEditMeeting = (state, data) => {
    state.editMeeting = JSON.parse(JSON.stringify(data))
}

const setPopupMeeting = (state, data) => {
    state.popupMeeting = data
    console.log(data)
}

export default {
    writeMyMeeting,
    clearMyMeeting,
    setModerateMeetings,
    clearModerateMeeting,
    clearMapMeeting,
    setMapMeetings,
    togglePreviewMeeting,
    writeChat,
    clearChat,
    addMessage,
    updatePeoples,
    setEditMeeting,
    setPopupMeeting
}