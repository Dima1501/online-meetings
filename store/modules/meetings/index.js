import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  myMeetings: [],
  moderateMeetings: [],
  mapMeetings: [],
  previewMeeting: {},
  chat: [],
  editMeeting: {},
  popupMeeting: {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};