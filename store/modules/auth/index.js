import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  restore: {
    email: ''
  },
  sign_in: {
    email: '',
    password: ''
  },
  registration: {
    email: '',
    password: '',
    name: '',
    coords: []
  },
  user: {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};